import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/dashboard",
      component: () => import("@/view/layout/Layout"),
      children: [
        {
          path: "/dashboard",
          name: "dashboard",
          component: () => import("@/view/pages/Dashboard.vue"),
        },
        {
          path: "/masterdata",
          name: "masterdata",
          component: () => import("@/view/layout/workspace/Comonrouter.vue"),
          children: [
            {
              path: "supplier",
              name: "supplier",
              component: () =>
                import("@/view/layout/workspace/supplier/Supplier.vue"),
            },
            {
              path: "supplier/store",
              name: "supplier_store",
              component: () => import("@/view/layout/workspace/supplier/Store.vue"),
            },
            {
              path: "supplier/store/:id",
              name: "supplier_store_id",
              component: () => import("@/view/layout/workspace/supplier/Store.vue"),
            },
            {
              path: "criteria",
              name: "criteria",
              component: () =>
                import("@/view/layout/workspace/criteria/criteria.vue"),
            },
            {
              path: "criteria/store",
              name: "criteria_store",
              component: () =>
                import("@/view/layout/workspace/criteria/store.vue"),
            },
          ],
        },
        {
          path: "/usermanagement",
          name: "usermanagement",
          component: () => import("@/view/layout/workspace/Comonrouter.vue"),
          children: [
            {
              path: "user",
              name: "user",
              component: () => import("@/view/layout/workspace/user/User.vue"),
            },
            {
              path: "user/store",
              name: "user_store",
              component: () => import("@/view/layout/workspace/user/Store.vue"),
            },
            {
              path: "user/store/:id",
              name: "user_store_id",
              component: () => import("@/view/layout/workspace/user/Store.vue"),
            },
            {
              path: "role",
              name: "role",
              component: () => import("@/view/layout/workspace/role/Role.vue"),
            },
            {
              path: "role/store",
              name: "role_store",
              component: () => import("@/view/layout/workspace/role/Store.vue"),
            },
            {
              path: "role/store/:id",
              name: "role_store_id",
              component: () => import("@/view/layout/workspace/role/Store.vue"),
            },
            {
              path: "features",
              name: "features",
              component: () =>
                import("@/view/layout/workspace/hakakses/Hakakses.vue"),
            },
            {
              path: "myprofile",
              name: "myprofile",
              component: () =>
                import("@/view/layout/workspace/myprofile/profile.vue"),
            },
          ],
        },
        {
          path: "/transaction",
          name: "transaction",
          component: () => import("@/view/layout/workspace/Comonrouter.vue"),
          children: [
            {
              path: "evaluation",
              name: "evaluation",
              component: () =>
                import("@/view/layout/workspace/evaluation/evaluation.vue"),
            },
            {
              path: "evaluation/store",
              name: "evaluation_store",
              component: () => import("@/view/layout/workspace/evaluation/Store.vue"),
            },
            {
              path: "evaluation/store/:id",
              name: "evaluation_store_id",
              component: () => import("@/view/layout/workspace/evaluation/Store.vue"),
            },
            {
              path: "archive_evaluation",
              name: "archive_evaluation",
              component: () =>
                import("@/view/layout/workspace/archive_evaluation/archive_evaluation.vue"),
            },
            {
              path: "archive_evaluation/detail/:id",
              name: "detail_archive_evaluation",
              component: () =>
                import("@/view/layout/workspace/archive_evaluation/detail.vue"),
            },
          ],
        },
      ],
    },
    {
      path: "/",
      component: () => import("@/view/pages/auth/login_pages/Login-1"),
      children: [
        {
          name: "login",
          path: "/login",
          component: () => import("@/view/pages/auth/login_pages/Login-1"),
        },
        {
          name: "register",
          path: "/register",
          component: () => import("@/view/pages/auth/login_pages/Login-1"),
        },
      ],
    },
    {
      path: "*",
      redirect: "/404",
    },
    {
      // the 404 route, when none of the above matches
      path: "/404",
      name: "404",
      component: () => import("@/view/pages/error/Error-1.vue"),
    },
  ],
});
