import Vue from "vue";
import { create } from "apisauce";
import JwtService from "@/core/services/jwt.service";
var apiSauces = "";

/**
 * Service to call HTTP request via Axios
 */
const ApiSauceService = {
  init() {
    // eslint-disable-next-line no-undef
    return (apiSauces = create({
      baseURL: Vue.prototype.$base_url,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json;charset"
      }
    }));
  },

  /**
   * Set the default HTTP request headers
   */
  setHeader() {
    apiSauces.setHeader("Authorization", `Token ${JwtService.getToken()}`);
  }
};

export default ApiSauceService;
